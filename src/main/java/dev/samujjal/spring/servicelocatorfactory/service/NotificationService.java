package dev.samujjal.spring.servicelocatorfactory.service;

import dev.samujjal.spring.servicelocatorfactory.domain.NotificationRequest;

public interface NotificationService {
    boolean notify(NotificationRequest notificationRequest);
}
