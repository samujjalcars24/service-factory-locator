package dev.samujjal.spring.servicelocatorfactory.service.impl;

import dev.samujjal.spring.servicelocatorfactory.domain.NotificationRequest;
import dev.samujjal.spring.servicelocatorfactory.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service("whatsapp")
@Slf4j
public class WhatsAppServiceImpl implements NotificationService {
    @Override
    public boolean notify(NotificationRequest notificationRequest) {
        log.info("notification via whatsapp: {}", notificationRequest);
        return true;
    }
}
