package dev.samujjal.spring.servicelocatorfactory.service.impl;

import dev.samujjal.spring.servicelocatorfactory.domain.NotificationRequest;
import dev.samujjal.spring.servicelocatorfactory.service.NotificationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service("sms")
@Slf4j
public class SMSServiceImpl implements NotificationService {
    @Override
    public boolean notify(NotificationRequest notificationRequest) {
        log.info("notification via SMS: {}", notificationRequest);
        return true;
    }
}
