package dev.samujjal.spring.servicelocatorfactory.service;

public interface NotificationRegistry {
    NotificationService getServiceBean(String serviceName);
}
