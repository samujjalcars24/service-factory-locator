package dev.samujjal.spring.servicelocatorfactory.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class NotificationRequest {
    private int dealerId;
    private String message;
}
