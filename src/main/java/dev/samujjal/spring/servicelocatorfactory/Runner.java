package dev.samujjal.spring.servicelocatorfactory;

import dev.samujjal.spring.servicelocatorfactory.domain.NotificationRequest;
import dev.samujjal.spring.servicelocatorfactory.service.NotificationRegistry;
import dev.samujjal.spring.servicelocatorfactory.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class Runner implements CommandLineRunner {

    @Autowired
    private NotificationRegistry notificationRegistry;

    @Override
    public void run(String... args) throws Exception {
        NotificationRequest notificationRequest = new NotificationRequest(100, "hello");

        NotificationService notificationService = notificationRegistry.getServiceBean("sms");
        notificationService.notify(notificationRequest);
    }
}
